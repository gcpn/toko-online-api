This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Membuat sebuah aplikasi yang memenuhi ketentuan berikut:

- (nanti dulu ya) Memiliki proses autentikasi dengan memanfaatkan Authentication API
- Menggunakan AJAX untuk mendapatkan data-data dinamis
- Menerapkan beberapa fitur pada React-DOM API
- Menggunakan animasi pada komponen dari data-data yang dinamis
