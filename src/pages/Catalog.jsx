import React, { Component } from 'react'
import { CSSTransitionGroup } from 'react-transition-group'
import './Catalog.css'

export default class Catalog extends Component {
    constructor(props) {
        super(props)
        this.state = {
            length: 0,
            products: []
        }
        this.handleAdd = this.handleAdd.bind(this)
    }

    handleAdd() {
        const newProducts = this.state.products.concat([
            {
                id: this.state.length + 1,
                title: prompt('Masukkan nama produk'),
                price: prompt('Masukkan harga produk')
            }
        ])
        this.setState((state) => ({
            length: state.length + 1,
            products: newProducts
        }))
    }

    handleRemove(id) {
        let newProducts = this.state.products.filter(
            (product) => product.id !== id
        )
        this.setState({
            products: newProducts
        })
    }

    componentDidMount() {
        // ketika komponen selesai di-mount, minta data produk via API
        fetch('https://fakestoreapi.com/products?limit=10')
            .then(res => res.json())
            .then(res => this.setState({
                length: res.length,
                products: res
            }))
    }

    render() {
        const { products } = this.state
        const productsItem = products.map((product) =>
            <div key={product.id} className="div-product m-3">
                <span onClick={() => this.handleRemove(product.id)} style={{ color: "red", cursor: "pointer" }}>X</span>
                <h3>{product.title}</h3>
                <img src={product.image} alt="gambar" className="img-product" />
                <div className="detail">
                    <b>Rp. {product.price * 14700}</b><br/>
                    {product.description}<br />
                </div>
                <hr />
            </div>
        )

        return (
            <React.Fragment>
                <div className="container">
                    <h2>Kelola produk</h2>
                    <button className="btn btn-primary" onClick={this.handleAdd} style={{ padding: "5px 20px" }}>Tambah produk</button>
                    <CSSTransitionGroup
                        transitionName="fade"
                        transitionEnterTimeout={500}
                        transitionLeaveTimeout={300}>
                        <div className="mt-3 d-flex flex-wrap">
                            {productsItem}
                        </div>
                    </CSSTransitionGroup>
                </div>
            </React.Fragment>
        )
    }
}