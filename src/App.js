import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Catalog from './pages/Catalog'
import NavBar from './components/NavBar'
import Footer from './components/Footer'

function App() {
  return (
    <React.Fragment>
      <NavBar />
      <Catalog />
      <Footer />
    </React.Fragment>
  );
}

export default App;
