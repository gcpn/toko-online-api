import React, { Component } from 'react'
import './Footer.css'

export default class Footer extends Component {
    render() {
        return (
            <div className="container-fluid text-center mt-3 py-3" id="footer">
                <p>&copy; 2020 Gottfried - DTS ITP UI FE</p>
            </div>
        )
    }
}
